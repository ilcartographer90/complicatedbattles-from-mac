﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplicatedBattles.Sprite
{
    public class BasicSprite
    {
        Texture2D spriteTexture;
        int spriteWidth;
        int spriteHeight;
        Point cellOffset;

        public BasicSprite(Texture2D spriteTexture, int spriteWidth, int spriteHeight, Point cellOffset)
        {
            this.spriteTexture = spriteTexture;
            this.spriteWidth = spriteWidth;
            this.spriteHeight = spriteHeight;
            this.cellOffset = cellOffset;
        }

        public void Update(GameTime gameTime)
        {
            // TODO: Update code (for animation)
        }

        public void Draw(SpriteBatch spriteBatch, Rectangle destination)
        {
            Draw(spriteBatch, destination, Color.White);
        }

        public void Draw(SpriteBatch spriteBatch, Rectangle destination, Color color)
        {
            Rectangle source = new Rectangle((int)(cellOffset.X * spriteWidth), (int)(cellOffset.Y * spriteHeight),
                                            (int)spriteWidth, (int)spriteHeight);
            spriteBatch.Draw(spriteTexture, destination, source, color);
        }
    }
}
