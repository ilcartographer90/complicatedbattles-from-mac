﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ComplicatedBattles.Input;

namespace ComplicatedBattles.Screen
{
    public delegate void ScreenEventDelegate(String param = "");

    public abstract class AbstractScreen
    {
        Dictionary<String, Tuple<String, bool>> screenTransitions = new Dictionary<String, Tuple<String, bool>>();
        public Dictionary<String, Tuple<String, bool>> ScreenTransitions
        {
            get { return screenTransitions; }
        }

        bool isPopup = false;
        public bool IsPopup
        {
            get { return isPopup; }
            set { isPopup = value; }
        }

        bool isActive = true;
        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        ScreenManager screenManager;
        public ScreenManager ScreenManager
        {
            get { return screenManager; }
            set { screenManager = value; }
        }

		/// <summary>
		/// Gets the index of the player who is currently controlling this screen,
		/// or null if it is accepting input from any player. This is used to lock
		/// the game to a specific player profile. The main menu responds to input
		/// from any connected gamepad, but whichever player makes a selection from
		/// this menu is given control over all subsequent screens, so other gamepads
		/// are inactive until the controlling player returns to the main menu.
		/// </summary>
		public PlayerIndex? ControllingPlayer
		{
			get { return controllingPlayer; }
			internal set { controllingPlayer = value; }
		}

		PlayerIndex? controllingPlayer;

        public virtual void Activate()
        {

        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(GameTime gameTime)
        {

        }

		public virtual void HandleInput(GameTime gameTime, InputState input) { }

        public void AddTransitionScreenEvent(string eventName, string className, bool isPopup)
        {
            ScreenTransitions.Add(eventName, new Tuple<string, bool>(className, isPopup));
        }

        protected void TransitionScreenEvent(string eventName)
        {
            Tuple<String, bool> screenProps = ScreenTransitions[eventName];
            ScreenManager.AddScreen(ScreenFactory.CreateScreen(screenProps.Item1, screenProps.Item2));
        }

        public virtual void Unload()
        {

        }

        public void ExitScreen()
        {
            ScreenManager.RemoveScreen();
        }

    }
}
