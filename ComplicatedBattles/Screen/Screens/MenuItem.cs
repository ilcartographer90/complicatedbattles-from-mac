﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace ComplicatedBattles.Screen.Screens
{
    class MenuItem
    {
        string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        bool isVisible;
        public bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; }
        }

		string onSelectParam;
		public string OnSelectParam
		{
			get { return onSelectParam; }
			set { onSelectParam = value; }
		}

		public MenuItem(string text, ScreenEventDelegate onSelect, string onSelectParam = "")
        {
            this.text = text;
			this.OnSelect = onSelect;
			this.OnSelectParam = onSelectParam;
        }

		public ScreenEventDelegate OnSelect;

        protected internal virtual void OnSelectEntry()
        {
			OnSelect (OnSelectParam);
        }

        public void Update(GameTime gameTime)
        {
            // TODO
        }

        public void Draw(AbstractMenuScreen parentScreen, bool isSelected, GameTime gameTime)
        {
			// Draw the selected entry in yellow, otherwise white.
			Color color = isSelected ? Color.Yellow : Color.White;

			float scale = 1;

			// Draw text, centered on the middle of each line.
			ScreenManager screenManager = parentScreen.ScreenManager;
			SpriteBatch spriteBatch = screenManager.SpriteBatch;
			SpriteFont font = screenManager.DefaultFont;

			Vector2 origin = new Vector2(0, font.LineSpacing / 2);

			spriteBatch.DrawString(font, text, position, color, 0,
				origin, scale, SpriteEffects.None, 0);
        }

		public virtual int GetHeight(AbstractMenuScreen screen)
		{
			return screen.ScreenManager.DefaultFont.LineSpacing;
		}

		public virtual int GetWidth(AbstractMenuScreen screen)
		{
			return (int)screen.ScreenManager.DefaultFont.MeasureString(Text).X;
		}

    }
}
