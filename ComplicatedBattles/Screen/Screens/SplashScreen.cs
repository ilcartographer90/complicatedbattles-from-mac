﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplicatedBattles.Screen.Screens
{
    class SplashScreen : AbstractScreen
    {
        ContentManager content;
        Texture2D backgroundTexture;

        double startTime;

        public override void Activate()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            backgroundTexture = content.Load<Texture2D>(@"Splashes\advance_wars");

            AddTransitionScreenEvent("splashDurationFinished", "ComplicatedBattles.Screen.Screens.MainMenuScreen", false);

            startTime = 0;
        }

        public override void Unload()
        {
            content.Unload();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            startTime += gameTime.ElapsedGameTime.TotalSeconds;

            if(startTime >= 3)
            {
                TransitionScreenEvent("splashDurationFinished");
            }
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);

            spriteBatch.Begin();

            spriteBatch.Draw(backgroundTexture, fullscreen, Color.White);

            spriteBatch.End();

        }
    }
}
