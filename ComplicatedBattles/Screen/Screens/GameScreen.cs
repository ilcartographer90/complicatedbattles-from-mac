﻿using System;
using ComplicatedBattles.Screen;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ComplicatedBattles.GameEngine;
using ComplicatedBattles.Component;
using ComplicatedBattles.Input;
using Microsoft.Xna.Framework.Input;
using ComplicatedBattles.Sprite;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.IO;

namespace ComplicatedBattles.Screen.Screens
{
	public class GameScreen : AbstractScreen
	{
		ContentManager content;

		Engine engine = new Engine(32, 32);

		Cursor cursor;

        Camera camera;

        TileMap theMap;

        Dictionary<int, BasicSprite> unitSprites = new Dictionary<int, BasicSprite>();
        Dictionary<int, String> unitTypes = new Dictionary<int, string>();

        // TODO: This needs to be handled in a much better way
        InputAction tileSelect;
        bool isUnitSelected;

        EntityManager entityManager;

        public Camera Camera
		{
			get { return camera; }
			set { camera = value; }
		}

        public GameScreen()
        {
            tileSelect = new InputAction(
                new Buttons[] { Buttons.A },
                new Keys[] { Keys.Select, Keys.A },
                true);
        }

        public void Initialize()
        {
            
        }

        public override void Activate()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            GameDataManager.Initialize();

            // TODO: TileMap property?
            Point tileSize = new Point(32, 32);
            
            // TODO: This should actually be a function parameter; A new state for map select needs to be added to transition into this screen
            theMap = new TileMap(content,"demo");
            Point mapTiles = new Point(theMap.MapWidth, theMap.MapHeight);

            // TODO: The cursor and camera are directly connected to each other, but I'm breaking it up for now
            cursor = new Cursor(ScreenManager.Input, Point.Zero, mapTiles);
            camera = new Camera(((Game1)ScreenManager.Game).ScreenRectangle, mapTiles, tileSize, cursor);

            cursor.LoadContent(content);

            entityManager = new EntityManager(this.ScreenManager.Game, camera, engine);
            entityManager.DrawOrder = 2;
            entityManager.Initialize();

            this.ScreenManager.Game.Components.Add(entityManager);

            LoadUnits(content, entityManager, "demo");
        }

		public override void Unload()
		{
			content.Unload();


		}

        private void LoadUnits(ContentManager content, EntityManager entityManager, String mapName)
        {
            // Load the sprites
            JObject mapObject = JObject.Parse(File.ReadAllText("Content/Maps/" + mapName + ".json"));
            
            // TODO: Handle all possible teams
            var spriteSheet = mapObject.GetValue("tilesets").First(ts => ts.Value<String>("name") == "RedUnits");

            String unitImageName = Path.GetFileNameWithoutExtension(spriteSheet.Value<String>("image"));

            Texture2D unitTexture = content.Load<Texture2D>("Sprites\\" + unitImageName);
            int unitWidth = spriteSheet.Value<int>("tilewidth");
            int unitHeight = spriteSheet.Value<int>("tileheight");

            // This is the value that the current tileset's indexes start at in the map data
            int spriteIdOffset = spriteSheet.Value<int>("firstgid");

            // Load each unit for the current sprite sheet
            int unitCounter = 0;
            foreach (JProperty prop in spriteSheet["tileproperties"])
            {
                BasicSprite unitSprite = new BasicSprite(unitTexture, unitWidth, unitHeight, new Point(unitCounter, 0));
                unitSprites.Add(unitCounter + spriteIdOffset, unitSprite);
                unitTypes.Add(unitCounter + spriteIdOffset, prop.Value["unit"].ToString());
                unitCounter++;
            }

            var layer = mapObject.GetValue("layers").First(l => l.Value<String>("name") == "unitLayer");

            int mapHeight = layer.Value<int>("height");
            int mapWidth = layer.Value<int>("width");
            List<int> data = layer["data"].ToObject<List<int>>();

            for (int j = 0; j < mapHeight; j++)
            {
                for (int i = 0; i < mapWidth; i++) {
                    int unitId = data[j * mapWidth + i];
                    if (data[j * mapWidth + i] == 0)
                        continue;

                    UnitPlayerEntity unit = new UnitPlayerEntity(unitSprites[unitId], ScreenManager.DefaultFont, new Point(i, j));
                    unit.UnitPrototype = GameDataManager.Units[unitTypes[unitId]];
                    unit.OnSelect = OnUnitSelect;

                    entityManager.AddEntity(unit);
                }
            }
        }

        private void OnUnitSelect(String transitionEventName, AbstractPlayerEntity selectedEntity)
        {
            isUnitSelected = !isUnitSelected;
        }

        public override void HandleInput(GameTime gameTime, InputState input)
        {
            PlayerIndex playerIndex;

            // Move to the previous menu entry?
            if (tileSelect.Evaluate(input, ControllingPlayer, out playerIndex))
            {
                // If a unit was selected, we're going to move it now
                if (isUnitSelected && !entityManager.EntityExistsAtLocation(cursor.SelectedTile))
                {
                    UnitPlayerEntity selectedEntity = entityManager.SelectedEntity as UnitPlayerEntity;

                    List<PathNode> path = theMap.CalculateMovement(selectedEntity.CellPosition, cursor.SelectedTile, selectedEntity);

                    // Todo: This really should be a check to see if the unit can move to this spot, which is just a little bit more involved
                    if (selectedEntity!= null && path != null && selectedEntity.TotalMovement >= path.Sum(n => n.MovementCost))
                    {
                        entityManager.SelectedEntity.CellPosition = cursor.SelectedTile;
                    }
                }

                entityManager.SelectEntityAtLocation(cursor.SelectedTile);
            }
            
        }

        public override void Update(GameTime gameTime)
		{
            cursor.Update(gameTime, ControllingPlayer);
            camera.Update();

            base.Update(gameTime);
		}

		public override void Draw(GameTime gameTime)
		{
			SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

			spriteBatch.Begin(SpriteSortMode.Immediate,
				BlendState.AlphaBlend,
				SamplerState.PointClamp,
				null,
				null,
				null,
				Matrix.Identity);

            //theMap.Draw(gameTime, spriteBatch, isUnitSelected);

            DrawMap(gameTime, spriteBatch, theMap, isUnitSelected);

			spriteBatch.End();
		}

        private void DrawMap(GameTime gameTime, SpriteBatch spriteBatch, TileMap map, bool isUnitSelected)
        {
            Rectangle destination = new Rectangle(0, 0, engine.TileWidth, engine.TileHeight);

            // Only draw the tiles that are within the viewport
            for (int y = (int)camera.FirstTile.Y; y < camera.TilesPerScreen.Y + camera.FirstTile.Y; y++)
            {
                destination.Y = y * engine.TileHeight - (int)camera.Position.Y;

                for (int x = (int)camera.FirstTile.X; x < camera.TilesPerScreen.X + camera.FirstTile.X; x++)
                {
                    Color shade = isUnitSelected ? Color.Green : Color.White;
                    destination.X = x * engine.TileWidth - (int)camera.Position.X;
                    spriteBatch.Draw(
                        map.Tileset.Texture,
                        destination,
                        map.GetTileAtLocation(new Point(x, y)).SourceRectangle,
                        shade);

                    if (cursor.SelectedTile.X == x && cursor.SelectedTile.Y == y)
                    {
                        cursor.Draw(spriteBatch, destination);
                    }
                }
            }
        }
	}
}

