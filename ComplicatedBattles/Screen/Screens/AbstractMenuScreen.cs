﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ComplicatedBattles.Input;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace ComplicatedBattles.Screen.Screens
{
	abstract class AbstractMenuScreen : AbstractScreen
	{
		List<MenuItem> menuEntries = new List<MenuItem> ();

		protected IList<MenuItem> MenuEntries {
			get { return menuEntries; }
		}

		int selectedEntry = 0;

		InputAction menuUp;
		InputAction menuDown;
		InputAction menuSelect;
		InputAction menuCancel;

		public AbstractMenuScreen ()
		{
			menuUp = new InputAction (
				new Buttons[] { Buttons.DPadUp, Buttons.LeftThumbstickUp }, 
				new Keys[] { Keys.Up },
				true);
			menuDown = new InputAction (
				new Buttons[] { Buttons.DPadDown, Buttons.LeftThumbstickDown },
				new Keys[] { Keys.Down },
				true);
			menuSelect = new InputAction (
				new Buttons[] { Buttons.A, Buttons.Start },
				new Keys[] { Keys.Enter, Keys.Space },
				true);
			menuCancel = new InputAction (
				new Buttons[] { Buttons.B, Buttons.Back },
				new Keys[] { Keys.Escape },
				true);
		}

		protected virtual void OnSelectEntry (int entryIndex)
		{
			menuEntries [entryIndex].OnSelectEntry ();
		}

		protected virtual void OnCancel ()
		{
			ExitScreen ();
		}

		/// <summary>
		/// Responds to user input, changing the selected entry and accepting
		/// or cancelling the menu.
		/// </summary>
		public override void HandleInput (GameTime gameTime, InputState input)
		{
			// For input tests we pass in our ControllingPlayer, which may
			// either be null (to accept input from any player) or a specific index.
			// If we pass a null controlling player, the InputState helper returns to
			// us which player actually provided the input. We pass that through to
			// OnSelectEntry and OnCancel, so they can tell which player triggered them.
			PlayerIndex playerIndex;

			// Move to the previous menu entry?
			if (menuUp.Evaluate (input, ControllingPlayer, out playerIndex)) {
				selectedEntry--;

				if (selectedEntry < 0)
					selectedEntry = menuEntries.Count - 1;
			}

			// Move to the next menu entry?
			if (menuDown.Evaluate (input, ControllingPlayer, out playerIndex)) {
				selectedEntry++;

				if (selectedEntry >= menuEntries.Count)
					selectedEntry = 0;
			}

			if (menuSelect.Evaluate (input, ControllingPlayer, out playerIndex)) {
				OnSelectEntry (selectedEntry);
			} else if (menuCancel.Evaluate (input, ControllingPlayer, out playerIndex)) {
				OnCancel ();
			}
		}

		public override void Update (GameTime gameTime)
		{
			// Displaying 3 entries at a time
			for (int i = 0; i < MathHelper.Min (MenuEntries.Count, 3); i++) {
				menuEntries [(selectedEntry + i) % MenuEntries.Count].Update (gameTime);
			}

			// TODO: Additional update logic?
		}

		protected virtual void UpdateMenuEntryLocations ()
		{
			// start at Y = 175; each X value is generated per entry
			// TODO: This should be based on the actual screen size
			Vector2 position = new Vector2 (0f, 175f);

			// update each menu entry's location in turn
			for (int i = 0; i < MathHelper.Min (MenuEntries.Count, 3); i++) {
				MenuItem menuEntry = menuEntries [(selectedEntry + i) % MenuEntries.Count];

				// each entry is to be centered horizontally
				position.X = ScreenManager.GraphicsDevice.Viewport.Width / 2 - menuEntry.GetWidth (this) / 2;

				// set the entry's position
				menuEntry.Position = position;

				// move down for the next entry the size of this entry
				position.Y += menuEntry.GetHeight (this);
			}
		}

		public override void Draw (GameTime gameTime)
		{
			UpdateMenuEntryLocations ();

			SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

			spriteBatch.Begin ();

			for (int i = 0; i < MathHelper.Min (MenuEntries.Count, 3); i++) {
				menuEntries [(selectedEntry + i) % MenuEntries.Count].Draw (this, i == 0, gameTime);
			}

			spriteBatch.End ();

			// TODO: Additional draw logic?
		}
	}
}
