﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplicatedBattles.Screen.Screens
{
    class MainMenuScreen : AbstractMenuScreen
    {
        ContentManager content;
        Texture2D backgroundTexture;

		public MainMenuScreen() 
			: base() {
			AddTransitionScreenEvent("splashOptionSelected", "ComplicatedBattles.Screen.Screens.SplashScreen", false);
			AddTransitionScreenEvent("newGameOptionSelected", "ComplicatedBattles.Screen.Screens.GameScreen", false);

            // This could technically be ScreenTransitionSelect
			MenuEntries.Add(new MenuItem ("Splash Screen", ScreenTransitionSelect, "splashOptionSelected"));
			MenuEntries.Add(new MenuItem ("New Game", ScreenTransitionSelect, "newGameOptionSelected"));
			MenuEntries.Add(new MenuItem("Exit", Exit));
		}

        public override void Activate()
        {
            if (content == null)
            {
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            }

            backgroundTexture = content.Load<Texture2D>(@"Splashes\CEGUI_example_game_menu");
        }

		public void ScreenTransitionSelect(string transitionName) {
			TransitionScreenEvent (transitionName);
		}

		public void Exit(string dummy) 
		{
			ScreenManager.Game.Exit ();
		}

        public override void Unload()
        {
            content.Unload();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);

            spriteBatch.Begin();

            spriteBatch.Draw(backgroundTexture, fullscreen, Color.White);

            spriteBatch.End();

			base.Draw (gameTime);
        }
    }
}
