﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplicatedBattles.Screen
{
    public class ScreenFactory
    {
        public static AbstractScreen CreateScreen(String screenType, bool isPopup)
        {
            AbstractScreen newScreen = Activator.CreateInstance(Type.GetType(screenType)) as AbstractScreen;
            newScreen.IsPopup = isPopup;

            return newScreen;
        }
    }
}
