﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using ComplicatedBattles.Input;

namespace ComplicatedBattles.Screen
{
    public class ScreenManager : DrawableGameComponent
    {
        List<AbstractScreen> screens = new List<AbstractScreen>();

        bool isInitialized;

		InputState input = new InputState();
        public InputState Input
        {
            get { return input; }
        }

        SpriteBatch spriteBatch;
        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        SpriteFont defaultFont;
        public SpriteFont DefaultFont
        {
            get { return defaultFont; }
        }

        public ScreenManager(Game game)
            : base(game)
        {

        }

        public override void Initialize()
        {
            base.Initialize();

            isInitialized = true;
        }

        protected override void LoadContent()
        {
            ContentManager content = Game.Content;

            spriteBatch = new SpriteBatch(GraphicsDevice);

			defaultFont = content.Load<SpriteFont>(@"Fonts\default_menu_font");
            
        }

        protected override void UnloadContent()
        {
            foreach(AbstractScreen screen in screens)
            {
                screen.Unload();
            }
        }

        public override void Update(GameTime gameTime)
        {
            /*for(int i = screens.Count - 1; i >= 0; --i)
            {
                screens[i].Update(gameTime);
            }*/
			input.Update();

			// An input may remove the last screen, so we need to check that we have a screen to update check
			if (screens.Count > 0) {
                screens[screens.Count - 1].HandleInput(gameTime, input);
                screens [screens.Count - 1].Update (gameTime);
				
			} else {
				Game.Exit ();
			}
        }

        public override void Draw(GameTime gameTime)
        {
            for (int i = 0; i < screens.Count; ++i)
            {
                // TODO: Probably want something to say if it hides the one below it
                screens[i].Draw(gameTime);
            }
        }

        public void AddScreen(AbstractScreen screen)
        {
            // If the screen is not a popup, the screen below it should be paused
            if (!screen.IsPopup)
            {
                RemoveScreen();
            }

            screen.ScreenManager = this;

            screen.Activate();
        
            screens.Add(screen);
        }

        public void RemoveScreen()
        {
            if(screens.Count > 0)
            {
                screens[screens.Count -1].Unload();
                screens.RemoveAt(screens.Count -1);
            }            
        }
    }
}
