﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace ComplicatedBattles.GameEngine
{
    /// <summary>
    /// A tileset is a texture composed of multiple tiles, which are used to compose a map
    /// </summary>
	public class Tileset
	{
        /// <summary>
        /// The loaded texture used for the tileset sheet
        /// </summary>
		Texture2D texture;

        /// <summary>
        /// The pixel width of each tile
        /// </summary>
		int tileWidthInPixels;

        /// <summary>
        /// The pixel height of each tile
        /// </summary>
		int tileHeightInPixels;

        /// <summary>
        /// The number of columns in the tileset
        /// </summary>
		int tilesWide;

        /// <summary>
        /// The number of rows in the tileset
        /// </summary>
		int tilesHigh;

        /// <summary>
        /// Rectangles that break the tileset up into the individual tiles
        /// </summary>
		Rectangle[] sourceRectangles;

		public Texture2D Texture
		{
			get { return texture; }
			private set { texture = value; }
		}
		public int TileWidth
		{
			get { return tileWidthInPixels; }
			private set { tileWidthInPixels = value; }
		}
		public int TileHeight
		{
			get { return tileHeightInPixels; }
			private set { tileHeightInPixels = value; }
		}
		public int TilesWide
		{
			get { return tilesWide; }
			private set { tilesWide = value; }
		}
		public int TilesHigh
		{
			get { return tilesHigh; }
			private set { tilesHigh = value; }
		}
		public Rectangle[] SourceRectangles
		{
			get { return (Rectangle[])sourceRectangles.Clone(); }
		}

		public Tileset(Texture2D image, int tilesWide, int tilesHigh, int tileWidth, int
			tileHeight)
		{
			Texture = image;
			TileWidth = tileWidth;
			TileHeight = tileHeight;
			TilesWide = tilesWide;
			TilesHigh = tilesHigh;
			int tiles = tilesWide * tilesHigh;
			sourceRectangles = new Rectangle[tiles];
			int tile = 0;
			for (int y = 0; y < tilesHigh; y++)
				for (int x = 0; x < tilesWide; x++)
				{
					sourceRectangles[tile] = new Rectangle(
						x * tileWidth,
						y * tileHeight,
						tileWidth,
						tileHeight);
					tile++;
				}
		}
	}
}

