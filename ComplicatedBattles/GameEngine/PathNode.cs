﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplicatedBattles.GameEngine
{
    public enum NodeState
    {
        NA,
        OPEN,
        CLOSED
    }

    class PathNode
    {
        public Point Location { get; private set; }
        public int TileType { get; private set; }
        public int? MovementCost { get; set; }
        public bool IsWalkable { get { return MovementCost != null; } }
        public NodeState State { get; set; }
        public int G { get; set; }
        public int H { get; set; }
        public int F { get { return this.G + this.H; } }
        public PathNode Parent { get; set; }
        public bool IsInPath { get; set; }

        public PathNode(Point location, int tileType, int heuristic, int? movementCost, PathNode parentNode)
        {
            this.Location = location;
            this.TileType = tileType;
            this.State = NodeState.NA;
            this.MovementCost = movementCost;
            this.Parent = parentNode;
        }

        public void ResetNode()
        {
            this.MovementCost = null;
            this.State = NodeState.NA;
            this.IsInPath = false;
            this.Parent = null;
            this.G = -1;
            this.H = -1;
        }

        public void Initialize(int heuristic, int? movementCost, PathNode parentNode)
        {
            this.H = heuristic;
            this.G = parentNode != null ? (int)movementCost + parentNode.G : (int)movementCost;
            this.MovementCost = movementCost;
            this.Parent = parentNode;
            this.State = NodeState.NA;
        }

        public List<PathNode> GetPath()
        {
            List<PathNode> path = new List<PathNode>();

            PathNode currentNode = this;

            while(currentNode != null)
            {
                path.Add(currentNode);

                currentNode = currentNode.Parent;
            }

            path.Reverse();

            foreach(PathNode node in path)
            {
                Console.WriteLine("Path: " + node.Location + " Cost: " + node.MovementCost + " Tile Type: " + node.TileType);
            }

            Console.WriteLine("End Path");

            return path;
        }
    }
}
