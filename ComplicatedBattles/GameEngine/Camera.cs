﻿using Microsoft.Xna.Framework;
using ComplicatedBattles.Component;

namespace ComplicatedBattles.GameEngine
{
	public class Camera
	{
		Point position, firstTile, lastTile;
        Point mapDimensionsInPixels, tileSize, tilesPerScreen;

		Rectangle viewportRectangle;

        readonly Cursor cursor;
        
        public Point Position
        {
            get { return position; }
        }

		public Point FirstTile 
		{
			get { return firstTile; }
		}

        public Point LastTile
        {
            get { return lastTile; }
        }

		public Point TilesPerScreen 
		{
			get { return tilesPerScreen; }
		}

		public Camera(Rectangle viewport, Point mapTiles, Point tileSize, Cursor cursor)
		{          
			viewportRectangle = viewport;

            mapDimensionsInPixels.X = mapTiles.X * tileSize.X;
            mapDimensionsInPixels.Y = mapTiles.Y * tileSize.Y;

            this.tileSize = tileSize;

            // Used to help "center" the cursor, sorta
            tilesPerScreen.X = viewport.Width / tileSize.X;
            tilesPerScreen.Y = viewport.Height / tileSize.Y;

            this.cursor = cursor;
		}

		public void Update() 
		{
            position.X = (cursor.SelectedTile.X - (tilesPerScreen.X / 2)) * tileSize.X;
            position.Y = (cursor.SelectedTile.Y - (tilesPerScreen.Y / 2)) * tileSize.Y;
            position.X = MathHelper.Clamp(position.X, 0, mapDimensionsInPixels.X - viewportRectangle.Width);
            position.Y = MathHelper.Clamp(position.Y, 0, mapDimensionsInPixels.Y - viewportRectangle.Height);

			firstTile.X = position.X / tileSize.X;
			firstTile.Y = position.Y / tileSize.Y;
            
            lastTile.X = firstTile.X + tilesPerScreen.X - 1;  
            lastTile.Y = firstTile.Y + tilesPerScreen.Y - 1;
		}
	}
}

