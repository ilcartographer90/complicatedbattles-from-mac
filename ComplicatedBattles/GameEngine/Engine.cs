﻿using System;

// Tile System based on tutorial from http://xnagpa.net/
using Microsoft.Xna.Framework;

namespace ComplicatedBattles.GameEngine
{
	public class Engine
	{
		int tileWidth;
		public int TileWidth {
			get { return tileWidth; }
		}

		int tileHeight;
		public int TileHeight {
			get { return tileHeight; }
		}
        
		public Engine(int tileWidth, int tileHeight) {
			this.tileWidth = tileWidth;
			this.tileHeight = tileHeight;
		}

		public Point VectorToCell(Vector2 position)
		{
			return new Point((int)position.X / tileWidth, (int)position.Y / tileHeight);
		}
	}
}

