﻿using ComplicatedBattles.Component;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ComplicatedBattles.GameEngine
{
    /// <summary>
    /// Class used to load the map layers in order to draw the game world.
    /// </summary>
    class TileMap
    {
        /// <summary>
        /// The tileset to be used for the map
        /// </summary>
        public Tileset Tileset { get; private set; }

        /// <summary>
        /// A mapping of each tile to an integer index
        /// </summary>
        SortedDictionary<int, Tile> tiles = new SortedDictionary<int, Tile>();

        /// <summary>
        /// The list of nodes in the current path being calculated
        /// </summary>
        List<PathNode> pathfindingNodes = new List<PathNode>();

        /// <summary>
        /// The map data, holding a node that stores tile information for each cell in the map, 
        /// including heuristic data for pathfinding
        /// </summary>
        PathNode[,] pathMap;

        public int MapHeight {
            get; set;
        }

        public int MapWidth
        {
            get; set;
        }

        /// <summary>
        /// New maps must be loaded from a file
        /// </summary>
        /// <param name="content"></param>
        /// <param name="mapName"></param>
        public TileMap(ContentManager content, String mapName)
        {
            LoadMap(content, mapName);
        }

        /// <summary>
        /// Generate a TileMap based on the data from the input file
        /// </summary>
        /// <param name="content"></param>
        /// <param name="mapName">The name of the map file, without path & extension</param>
        public void LoadMap(ContentManager content, String mapName)
        {
            JObject mapObject = JObject.Parse(File.ReadAllText("Content/Maps/" + mapName + ".json"));

            var tileSet = mapObject.GetValue("tilesets").First(ts => ts.Value<String>("name") == "Base");

            String tileSetImage = Path.GetFileNameWithoutExtension(tileSet.Value<String>("image"));

            Texture2D tilesetTexture = content.Load<Texture2D>("Sprites\\" + tileSetImage);
            this.Tileset = new Tileset(tilesetTexture, tileSet.Value<int>("columns"), 1, tileSet.Value<int>("tilewidth"), tileSet.Value<int>("tileheight"));  // TODO: Rows?

            // TODO: What if tileset has multiple rows?
            int tileCounter = 0;
            foreach (JProperty prop in tileSet["tileproperties"])
            {
                Tile tile = new Tile(new Point(tileCounter, 0), this.Tileset, GameDataManager.Terrain[prop.Value["terrainType"].ToString()].Movement);
                tiles.Add(tileCounter, tile);
                tileCounter++;
            }

            // Load the movement layer
            var layer = mapObject.GetValue("layers").First(l => l.Value<String>("name") == "movementLayer");

            this.MapHeight = layer.Value<int>("height");
            this.MapWidth = layer.Value<int>("width");
            List<int> data = layer["data"].ToObject<List<int>>();

            pathMap = new PathNode[MapWidth, MapHeight];
            
            // Initialize the pathMap based on the map data
            for (int j = 0; j < MapHeight; ++j)
            {
                for (int i = 0; i < MapWidth; ++i)
                {
                    pathMap[i, j] = new PathNode(new Point(i, j), data[j * MapWidth + i] - 1, -1, null, null); // TODO: Have to offset by one here because of the way the data's exported?
                }
            }

        }

        public Tile GetTileAtLocation(Point location)
        {
            return tiles[pathMap[location.X, location.Y].TileType];
        }

        public Tile GetTileByKey(int key)
        {
            return tiles[key];
        }

        /// <summary>
        /// Reset all of the pathfinding data
        /// </summary>
        public void ClearPathFinding()
        {
            for(int j = 0; j < MapHeight; ++j)
            {
                for(int i = 0; i < MapWidth; ++i)
                {
                    pathMap[i, j].ResetNode();
                }
            }
        }

        /// <summary>
        /// Calculate the path that a unit can take to reach a given point
        /// </summary>
        /// <remarks>
        /// This calculation factors in the movement type of the unit and the movement cost associated with the terrain
        /// </remarks>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        public List<PathNode> CalculateMovement(Point start, Point end, UnitPlayerEntity unit)
        {
            pathfindingNodes.Clear();
            ClearPathFinding();
            
            if(end.X < 0 || end.X >= MapWidth || end.Y < 0 || end.Y >= MapHeight || GetTileAtLocation(end).MovementCosts[unit.UnitPrototype.MovementType] == null)
            {
                Console.WriteLine("Target tile is either not within the bounds or is an invalid tile");
                return null;
            }

            PathNode startNode = pathMap[start.X, start.Y];
            startNode.Initialize(GetManhattenDistance(start, end), 0, null);

            pathfindingNodes.Add(startNode);

            List<PathNode> openNodes = new List<PathNode>();

            openNodes.Add(startNode);

            while(openNodes.Count > 0)
            {

                // Get node with lowest F
                openNodes.Sort((node1, node2) => node1.F.CompareTo(node2.F));
                PathNode currentNode = openNodes[0];

                // If node is target node, return that node
                if(currentNode.Location == end)
                {
                    return currentNode.GetPath();
                }

                // Drop node from openNodes
                openNodes.Remove(currentNode);

                // Add node to closedNodes
                currentNode.State = NodeState.CLOSED;
                //closedNodes.Add(currentNode);

                // For each adjacent node
                foreach(PathNode adjNode in GetAdjacentNodes(currentNode, unit.UnitPrototype.MovementType, end))
                {
                    // If the node is not closed and is walkable
                    if(adjNode.IsWalkable && adjNode.State != NodeState.CLOSED)
                    {
                        // If it is not in the open list,
                        if (adjNode.State == NodeState.NA)
                        {
                            adjNode.State = NodeState.OPEN;
                            // Add it to opened list
                            openNodes.Add(adjNode);
                        }
                        else
                        {
                            // If the current node provides a better path to this adjacent node
                            if (currentNode.G + adjNode.MovementCost < adjNode.G)
                            {
                                // Change parent to current node and recalculate G
                                adjNode.Parent = currentNode;
                                adjNode.G = currentNode.G + (int)adjNode.MovementCost;
                            }

                        }

                    }

                }
            }

            Console.WriteLine("No path found");

            return null;
        }
                
        /// <summary>
        /// Get the nodes next to the specified node
        /// </summary>
        /// <param name="parent">The node to get the neighbors of</param>
        /// <param name="movementType">The movement type of the unit trying to move, used to check that the unit can actually use the adjacent node</param>
        /// <param name="endLocation">The selected destination of the unit</param>
        /// <returns></returns>
        // TODO: This is very ugly, let's redo it sometime
        // PathNode could have the tile type and we could make the map be an array of PathNodes
        private List<PathNode> GetAdjacentNodes(PathNode parent, MovementType movementType, Point endLocation)
        {
            List<PathNode> adjacentNodes = new List<PathNode>();
            Point parentLocation = parent.Location;
            
            if(parent.Location.X > 0)
            {
                Point leftLocation = new Point(parentLocation.X - 1, parentLocation.Y);
                int? movementCost = GetTileAtLocation(leftLocation).MovementCosts[movementType];

                if (movementCost != null && pathMap[leftLocation.X, leftLocation.Y].State == NodeState.NA)
                {
                    pathMap[leftLocation.X, leftLocation.Y].Initialize(GetManhattenDistance(leftLocation, endLocation), movementCost, parent);
                    
                    if(leftLocation == endLocation)
                    {
                        return new List<PathNode>() { pathMap[leftLocation.X, leftLocation.Y] };
                    }
                    else
                    {
                        adjacentNodes.Add(pathMap[leftLocation.X, leftLocation.Y]);
                    }
                }
            }

            if (parent.Location.X < MapWidth - 1)
            {
                Point rightLocation = new Point(parentLocation.X + 1, parentLocation.Y);
                int? movementCost = GetTileAtLocation(rightLocation).MovementCosts[movementType];

                if (movementCost != null && pathMap[rightLocation.X, rightLocation.Y].State == NodeState.NA)
                {
                    pathMap[rightLocation.X, rightLocation.Y].Initialize(GetManhattenDistance(rightLocation, endLocation), movementCost, parent);

                    if (rightLocation == endLocation)
                    {
                        return new List<PathNode>() { pathMap[rightLocation.X, rightLocation.Y] };
                    }
                    else
                    {
                        adjacentNodes.Add(pathMap[rightLocation.X, rightLocation.Y]);
                    }
                }
            }

            if (parent.Location.Y > 0)
            {
                Point upLocation = new Point(parentLocation.X, parentLocation.Y - 1);
                int? movementCost = GetTileAtLocation(upLocation).MovementCosts[movementType];

                if (movementCost != null && pathMap[upLocation.X, upLocation.Y].State == NodeState.NA)
                {
                    pathMap[upLocation.X, upLocation.Y].Initialize(GetManhattenDistance(upLocation, endLocation), movementCost, parent);

                    if (upLocation == endLocation)
                    {
                        return new List<PathNode>() { pathMap[upLocation.X, upLocation.Y] };
                    }
                    else
                    {
                        adjacentNodes.Add(pathMap[upLocation.X, upLocation.Y]);
                    }
                }
            }

            if (parent.Location.Y < MapHeight - 1)
            {
                Point downLocation = new Point(parentLocation.X, parentLocation.Y + 1);
                int? movementCost = GetTileAtLocation(downLocation).MovementCosts[movementType];

                if (movementCost != null && pathMap[downLocation.X, downLocation.Y].State == NodeState.NA)
                {
                    pathMap[downLocation.X, downLocation.Y].Initialize(GetManhattenDistance(downLocation, endLocation), movementCost, parent);

                    if (downLocation == endLocation)
                    {
                        return new List<PathNode>() { pathMap[downLocation.X, downLocation.Y] };
                    }
                    else
                    {
                        adjacentNodes.Add(pathMap[downLocation.X, downLocation.Y]);
                    }
                }
            }


            return adjacentNodes;
        }

        /// <summary>
        /// Get the Manhatten distance between two given points
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private int GetManhattenDistance(Point start, Point end)
        {
            return Math.Abs(end.X - start.X) + Math.Abs(end.Y - start.Y);
        }
    }
}
