﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace ComplicatedBattles.GameEngine
{
	public class Tile
	{
		Point tileIndex;
		Tileset tileset;
        
        Dictionary<MovementType, int?> movementCosts;

		public Point TileIndex
		{
			get { return tileIndex; }
			private set { tileIndex = value; }
		}

		public Tileset Tileset
		{
			get { return tileset; }
			private set { tileset = value; }
		}

        public Dictionary<MovementType, int?> MovementCosts
        {
            get { return movementCosts; }
            private set { movementCosts = value; }
        }

        public Rectangle SourceRectangle
        {
            get
            {
                return tileset.SourceRectangles[TileIndex.Y * Tileset.TilesWide + TileIndex.X];
            }
        }

		public Tile(Point tileIndex, Tileset tileset, Dictionary<MovementType, int?> movementCosts)
		{
			TileIndex = tileIndex;
			Tileset = tileset;
            MovementCosts = movementCosts;
            
		}
	}
}

