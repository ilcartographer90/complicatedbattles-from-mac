﻿using ComplicatedBattles.GameEngine;
using ComplicatedBattles.Component.Prototypes;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ComplicatedBattles.Component
{
    // TODO: Probably should just make a general map layer, since the draw code is going to be recycled a lot :)
    class EntityManager : DrawableGameComponent
    {
        bool isInitialized;
        SpriteBatch spriteBatch;

        Engine engine;
        Camera camera;
        
        List<AbstractPlayerEntity> entityList;

        AbstractPlayerEntity selectedEntity;

        public AbstractPlayerEntity SelectedEntity
        {
            get { return selectedEntity; }
        }
        
        public EntityManager(Game game, Camera camera, Engine engine)
            : base(game)
        {
            this.camera = camera;
            this.engine = engine;

            entityList = new List<AbstractPlayerEntity>();
        }

        public override void Initialize()
        {
            base.Initialize();

            
            isInitialized = true;
        }

        protected override void LoadContent()
        {
            ContentManager content = Game.Content;

            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
        }

        public void AddEntity(AbstractPlayerEntity entity)
        {
            entityList.Add(entity);
        }

        public bool EntityExistsAtLocation(Point cellPosition)
        {
            return entityList.Exists(x => x.CellPosition == cellPosition);
        }

        public AbstractPlayerEntity GetEntityAtLocation(Point cellPosition)
        {
            return entityList.Find(x => x.CellPosition == cellPosition);
        }

        // TODO: This will not be good enough once we're ready to add buildings
        // Definitely going to end up redoing this a bunch :)
        public void SelectEntityAtLocation(Point cellPosition)
        {
            AbstractPlayerEntity selectedEntity = GetEntityAtLocation(cellPosition);

            if(selectedEntity != null)
            {
                this.selectedEntity = selectedEntity;
                selectedEntity.SelectEntity();
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach(AbstractPlayerEntity entity in entityList)
            {
                entity.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                null,
                null,
                null,
                Matrix.Identity);

            foreach (AbstractPlayerEntity entity in entityList)
            {
                // temp variables to alias these
                int cellX = entity.CellPosition.X;
                int cellY = entity.CellPosition.Y;
                if(cellX >= camera.FirstTile.X && cellY <= camera.LastTile.Y)
                {
                    Rectangle destination = new Rectangle(cellX * engine.TileWidth - (int)camera.Position.X, 
                                                            cellY * engine.TileHeight - (int)camera.Position.Y, 
                                                            engine.TileWidth, 
                                                            engine.TileHeight);

                    entity.Draw(gameTime, spriteBatch, destination);
                }
            }

            spriteBatch.End();
        }
    }
}