﻿using ComplicatedBattles.Component.Prototypes;
using ComplicatedBattles.Component.ProtoTypes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace ComplicatedBattles.Component
{
    class GameDataManager
    {
        public static Dictionary<String, TerrainPrototype> Terrain;
        public static Dictionary<String, UnitPrototype> Units;

        private static T LoadPrototype<T>(String fileName)
        {
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(fileName));
        }

        public static void Initialize()
        {
            Units = LoadPrototype<Dictionary<String, UnitPrototype>>(@"Content/Data/units.json");
            Terrain = LoadPrototype<Dictionary<String, TerrainPrototype>>(@"Content/Data/terrain.json");
        }
    }
}
