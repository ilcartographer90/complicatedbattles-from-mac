﻿using System;
using ComplicatedBattles.GameEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ComplicatedBattles.Input;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ComplicatedBattles.Component
{
	public class Cursor
	{
        Point selectedTile;
        Point mapSizeInTiles;

        InputState inputState;

        InputAction scrollUp;
        InputAction scrollDown;
        InputAction scrollLeft;
        InputAction scrollRight;

        double delay = 100;
        double elapsedTime = 0;

        Texture2D cursorTexture;

        public Point SelectedTile
        {
            get { return selectedTile; }
        }

        public Cursor (InputState inputState, Point selectedTile, Point mapSizeInTiles)
		{
            this.inputState = inputState;
            this.selectedTile = selectedTile;
            this.mapSizeInTiles = mapSizeInTiles;

            InitializeInputActions();
		}

        public void LoadContent(ContentManager content)
        {
            cursorTexture = content.Load<Texture2D>(@"Sprites\cursor");
        }

        private void InitializeInputActions()
        {
            scrollUp = new InputAction(
                new Buttons[] { Buttons.DPadUp, Buttons.LeftThumbstickUp },
                new Keys[] { Keys.Up },
                false);
            scrollDown = new InputAction(
                new Buttons[] { Buttons.DPadDown, Buttons.LeftThumbstickDown },
                new Keys[] { Keys.Down },
                false);
            scrollLeft = new InputAction(
                new Buttons[] { Buttons.DPadLeft, Buttons.LeftThumbstickLeft },
                new Keys[] { Keys.Left },
                false);
            scrollRight = new InputAction(
                new Buttons[] { Buttons.DPadRight, Buttons.LeftThumbstickRight },
                new Keys[] { Keys.Right },
                false);
        }

        public void Update(GameTime gameTime, PlayerIndex? controllingPlayer)
		{
            if (elapsedTime > delay)
            {
                PlayerIndex playerIndex;
                if (scrollUp.Evaluate(inputState, controllingPlayer, out playerIndex))
                {
                    --selectedTile.Y;
                }
                else if (scrollDown.Evaluate(inputState, controllingPlayer, out playerIndex))
                {
                    ++selectedTile.Y;
                }

                if (scrollLeft.Evaluate(inputState, controllingPlayer, out playerIndex))
                {
                    --selectedTile.X;
                }
                else if (scrollRight.Evaluate(inputState, controllingPlayer, out playerIndex))
                {
                    ++selectedTile.X;
                }

                selectedTile.X = MathHelper.Clamp(selectedTile.X, 0, mapSizeInTiles.X - 1);
                selectedTile.Y = MathHelper.Clamp(selectedTile.Y, 0, mapSizeInTiles.Y - 1);

                elapsedTime = 0;
            }

            elapsedTime += gameTime.ElapsedGameTime.TotalMilliseconds;            
        }
        
        public void Draw(SpriteBatch spriteBatch, Rectangle destination)
        {
            spriteBatch.Draw(cursorTexture, destination, Color.White);
        }
	}
}

