﻿using ComplicatedBattles.GameEngine;
using System.Collections.Generic;

namespace ComplicatedBattles.Component.ProtoTypes
{
    class TerrainPrototype
    {
        public Dictionary<MovementType, int?> Movement { get; set; }
    }
}
