﻿using ComplicatedBattles.GameEngine;

namespace ComplicatedBattles.Component.Prototypes
{
    class UnitPrototype
    {
        public int Health { get; set; }
        public int Movement { get; set; }
        public int Strength { get; set; }
        public int Armor { get; set; }
        public MovementType MovementType { get; set; }
    }
}
