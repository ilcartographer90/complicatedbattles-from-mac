﻿using ComplicatedBattles.Screen;
using ComplicatedBattles.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComplicatedBattles.Component
{
    delegate void EntitySelectDelegate(String transitionEventName, AbstractPlayerEntity selectedEntity);

	// A player entity is anything that is rendered on top of the terrain that can belong to a player
    abstract class AbstractPlayerEntity
    {
        protected Point cellPosition;
        protected BasicSprite entitySprite;
        protected SpriteFont font;
        // TODO: player/color


        public EntitySelectDelegate OnSelect;

        public Point CellPosition
        {
            get { return cellPosition; }
            set { cellPosition = value; }
        }

        public abstract String TransitionEventName
        {
            get;
        }

		public AbstractPlayerEntity(BasicSprite sprite, SpriteFont font, Point initialPosition)
        {
            this.entitySprite = sprite;
            this.font = font;
            this.cellPosition = initialPosition;
        }

        public void SelectEntity()
        {
            OnSelect(TransitionEventName, this);
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch, Rectangle destination)
        {
            entitySprite.Draw(spriteBatch, destination);
        }

    }
}
