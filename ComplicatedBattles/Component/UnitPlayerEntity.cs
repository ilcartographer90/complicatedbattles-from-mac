﻿using ComplicatedBattles.Component.Prototypes;
using ComplicatedBattles.Sprite;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace ComplicatedBattles.Component
{
    class UnitPlayerEntity : AbstractPlayerEntity
    {
        public UnitPrototype UnitPrototype { get; set; }

        public int MovementModifier { get; set; }

        public int DamageTaken { get; set; }

        public int CurrentHealth
        {
            get { return UnitPrototype.Health - DamageTaken; }
        }

        public int TotalMovement
        {
            get { return UnitPrototype.Movement + MovementModifier; }
        }

        public override string TransitionEventName
        {
            get
            {
                return "unitSelected";
            }
        }

		public UnitPlayerEntity(BasicSprite sprite, SpriteFont font, Point startingPosition)
            : base(sprite, font, startingPosition)
        {

        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch, Rectangle destination)
        {
            base.Draw(gameTime, spriteBatch, destination);

            // Going to draw the health in the bottom right quadrant of the sprite
            Rectangle healthRectangle = new Rectangle(destination.Center, new Point(destination.Size.X / 4, destination.Size.Y / 4));

            Vector2 dmgSize = font.MeasureString(CurrentHealth.ToString());

            // This scaling made it too small if it has 2 digits, so just going to do it big
            /*
            float xScale = healthRectangle.Width / dmgSize.X;
            float yScale = healthRectangle.Height / dmgSize.Y;

            float scale = Math.Min(xScale, yScale);
            */

            spriteBatch.DrawString(font, CurrentHealth.ToString(), healthRectangle.Location.ToVector2(), Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);

        }

        public void KillUnit()
        {
            // TODO: Play unit death animation, remove unit from game
        }

        public void ReceiveAttackFromUnit(UnitPlayerEntity otherUnit)
        {
            DamageTaken += Math.Min(otherUnit.UnitPrototype.Strength - this.UnitPrototype.Armor, 1);

            if(CurrentHealth <= 0)
            {
                KillUnit();
            }
        }
    }
}
